from PIL import Image, ImageDraw
import random
import math
import numpy as np


def calculateDistance(x1,y1,x2,y2):  
     dist = math.sqrt((x2 - x1)**2 + (y2 - y1)**2)  
     return dist


sizeX = 100
sizeY = 100




def generate(type, size):
    array_size = size

    distances = np.empty(array_size)

    def gen_image(index):
        im = Image.new("RGB", (sizeX, sizeY), (255,255,255))
        draw = ImageDraw.Draw(im)

        startX = random.randint(1, sizeX-1)
        startY = random.randint(1, sizeY-1)
        endX = random.randint(1, sizeX-1)
        endY = random.randint(1, sizeY-1)
        
        draw.line((startX, startY, endX, endY), fill=255)

        distance = calculateDistance(startX, startY, endX, endY)
        distances[index] = distance

        print(startX, endX, startY, endY, distance)

        im.save("{}_img/img_{}.jpg".format(type, str(index).zfill(4)))


    for i in range(array_size):
        gen_image(i)

    np.savetxt("{}_labels.txt".format(type), distances)


generate("train", 6000)
generate("val", 1000)
generate("test", 1000)