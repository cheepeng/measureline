from __future__ import absolute_import, division, print_function, unicode_literals
import tensorflow as tf
import numpy as np
import glob2 as glob
import IPython.display as display
import matplotlib.pyplot as plt
import math
from tensorflow import keras
from keras.backend.tensorflow_backend import set_session
import os

img_size = 100
learning_rate = 0.05
BATCH_SIZE = 4
epochs = 1000
tf.enable_eager_execution()

# config = tf.ConfigProto()
# config.gpu_options.allow_growth = True
# config.log_device_placement = True
# sess = tf.Session(config = config)

# gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.8)
# sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
# set_session(sess)

def preprocess_image(image):
  image = tf.image.decode_jpeg(image, channels=3)
  image = tf.image.resize(image, [img_size, img_size])
  image = tf.image.rgb_to_grayscale(image)
  image /= 255.0  # normalize to [0,1] range

  return image

def load_and_preprocess_image(path):
  image = tf.read_file(path)
  return preprocess_image(image)

AUTOTUNE = tf.data.experimental.AUTOTUNE

train_labels = np.loadtxt("train_labels.txt")
std = np.std(train_labels)
mean = np.mean(train_labels)
print("std dev", std)
print("mean", mean)

# train_labels = (train_labels - mean)/std

val_labels = np.loadtxt("val_labels.txt")
# val_labels = (val_labels - mean)/std

test_labels = np.loadtxt("test_labels.txt")
# test_labels = (test_labels - mean)/std


# print("std train", np.std(train_labels))
# print("std val", np.std(val_labels))
# print("std test", np.std(test_labels))

# print("var train", np.var(train_labels))
# print("var val", np.var(val_labels))
# print("var test", np.var(test_labels))

train_images = sorted(glob.glob("train_img/img_*.jpg"))
val_images = sorted(glob.glob("val_img/img_*.jpg"))
test_images = sorted(glob.glob("test_img/img_*.jpg"))

train_path_ds = tf.data.Dataset.from_tensor_slices(train_images)
val_path_ds = tf.data.Dataset.from_tensor_slices(val_images)

train_image_ds = train_path_ds.map(load_and_preprocess_image, num_parallel_calls = AUTOTUNE)
train_label_ds = tf.data.Dataset.from_tensor_slices(tf.cast(train_labels, tf.float32))
train_image_label_ds = tf.data.Dataset.zip((train_image_ds, train_label_ds))

val_image_ds = val_path_ds.map(load_and_preprocess_image, num_parallel_calls = AUTOTUNE)
val_label_ds = tf.data.Dataset.from_tensor_slices(tf.cast(val_labels, tf.float32))
val_image_label_ds = tf.data.Dataset.zip((val_image_ds, val_label_ds))


model = tf.keras.models.Sequential([
    tf.keras.layers.Convolution2D(4, 1, 1, input_shape=(img_size, img_size, 1), activation = 'relu'),
    tf.keras.layers.Convolution2D(4, 3, 1, activation = 'relu'),
    # tf.keras.layers.BatchNormalization(momentum = 0.9),
    # tf.keras.layers.Dropout(0.2),
    tf.keras.layers.MaxPooling2D(pool_size=(2,2)),
    tf.keras.layers.Convolution2D(8, 2, 1, activation = 'relu'),
    # tf.keras.layers.BatchNormalization(momentum = 0.9),
    # tf.keras.layers.Dropout(0.2),
    tf.keras.layers.MaxPooling2D(pool_size=(2,2)),
    tf.keras.layers.Convolution2D(16, 1, 1, activation = 'relu'),
    # tf.keras.layers.BatchNormalization(momentum = 0.9),
    # tf.keras.layers.Dropout(0.2),
    # tf.keras.layers.MaxPooling2D(pool_size=(2,2)),
    # tf.keras.layers.Convolution2D(64, 1, 1, activation = 'relu'),
    # tf.keras.layers.MaxPooling2D(pool_size=(2,2)),
    tf.keras.layers.Flatten(),
    # tf.keras.layers.Dense(500, activation=tf.nn.relu),
    # tf.keras.layers.Dropout(0.2),
    # tf.keras.layers.Dense(200, activation=tf.nn.relu),
    # tf.keras.layers.Dropout(0.2),
    # tf.keras.layers.Dense(64, activation=tf.nn.relu),
    # tf.keras.layers.Dropout(0.3),
    # tf.keras.layers.Dense(4, activation=tf.nn.relu),
    # tf.keras.layers.Dropout(0.5),
    # tf.keras.layers.Dense(4, activation=tf.nn.relu), #, kernel_regularizer = tf.keras.regularizers.l2(0.001)
    # tf.keras.layers.Dropout(0.5),
    # tf.keras.layers.Dense(8, activation=tf.nn.relu), #, kernel_regularizer = tf.keras.regularizers.l2(0.001)
    # tf.keras.layers.Dropout(0.5),
    # tf.keras.layers.Dense(4, activation=tf.nn.relu), #, kernel_regularizer = tf.keras.regularizers.l2(0.001)
    # tf.keras.layers.Dropout(0.5),
    # tf.keras.layers.Dense(10, activation=tf.nn.relu), #, kernel_regularizer = tf.keras.regularizers.l2(0.001)
    # tf.keras.layers.Dropout(0.5),
    # tf.keras.layers.Dense(2, activation=tf.nn.relu), #, kernel_regularizer = tf.keras.regularizers.l2(0.001)
    
    # tf.keras.layers.Dropout(0.5),
    # tf.keras.layers.Dense(100, activation=tf.nn.relu, kernel_regularizer = tf.keras.regularizers.l2(0.001)),
    # tf.keras.layers.Dropout(0.05),
    # tf.keras.layers.BatchNormalization(momentum = 0.9),
    tf.keras.layers.Dense(1, activation="linear")
])

model.compile(optimizer=tf.keras.optimizers.Adagrad(learning_rate),
    loss = "mean_squared_error",
    metrics = ["mean_absolute_error", "mean_squared_error"]
    )

train_ds = train_image_label_ds.cache(filename='./cache.train-data')
train_ds = train_ds.apply(tf.data.experimental.shuffle_and_repeat(buffer_size=len(train_images)))
train_ds = train_ds.batch(BATCH_SIZE)
train_ds = train_ds.prefetch(buffer_size=AUTOTUNE)


val_ds = val_image_label_ds.apply(
  tf.data.experimental.shuffle_and_repeat(buffer_size=len(val_images)))
val_ds = val_ds.batch(BATCH_SIZE)
val_ds = val_ds.prefetch(buffer_size=AUTOTUNE)

print(model.summary())


checkpoint_path = "checkpoint/cp.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)
latest = tf.train.latest_checkpoint(checkpoint_dir)

cp_callback = tf.keras.callbacks.ModelCheckpoint(checkpoint_path, save_weights_only = True, verbose = 1, period = 5)

#model.load_weights(latest)

history = model.fit(
    train_ds, 
    epochs = epochs, 
    validation_data = val_ds,
    steps_per_epoch = math.ceil(len(train_images)/BATCH_SIZE),
    validation_steps = math.ceil(len(val_images)/BATCH_SIZE),
    callbacks = [cp_callback]
    )


# plot train vs val
plt.figure(figsize=(16,10))
plt.plot(history.epoch, history.history['val_mean_squared_error'], '--', label = "val")
plt.plot(history.epoch, history.history['mean_squared_error'], label = "train")
plt.xlabel("Epochs")
plt.ylabel("mse")
plt.legend()
plt.show()


# test images
test_path_ds = tf.data.Dataset.from_tensor_slices(test_images)
test_image_ds = test_path_ds.map(load_and_preprocess_image, num_parallel_calls = AUTOTUNE)
test_label_ds = tf.data.Dataset.from_tensor_slices(tf.cast(test_labels, tf.float32))
test_image_label_ds = tf.data.Dataset.zip((test_image_ds, test_label_ds))

# test_ds = test_image_label_ds.apply()
  # tf.data.experimental.shuffle_and_repeat(buffer_size=len(test_images)))
test_ds = test_image_label_ds.repeat()
test_ds = test_ds.batch(BATCH_SIZE)
test_ds = test_ds.prefetch(buffer_size=AUTOTUNE)

model.evaluate(test_ds, steps = math.ceil(len(test_images)/BATCH_SIZE))



test_predictions = model.predict(test_ds, steps = math.ceil(len(test_images)/BATCH_SIZE)).flatten()
print("test labels count", test_labels[:30])
print("test prediction", test_predictions[:30])
# test_predictions = test_predictions * std + mean
# test_labels = test_labels * std + mean
plt.scatter(test_labels, test_predictions[:1000])
plt.xlabel('True Values')
plt.ylabel('Predictions')
plt.axis('equal')
plt.axis('square')
plt.xlim([0,plt.xlim()[1]])
plt.ylim([0,plt.ylim()[1]])
_ = plt.plot([0, 0], [-0, 0])
plt.show()
